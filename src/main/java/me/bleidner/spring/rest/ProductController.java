package me.bleidner.spring.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import me.bleidner.spring.model.Product;
import me.bleidner.spring.service.ProductService;


@RestController
public class ProductController {

    public static final String PRODUCTS_URL = "/products";

    @Autowired
    private ProductService productService;

    @GetMapping(PRODUCTS_URL)
    public List<Product> getProducts() {

        return productService.getProducts();
    }

}
