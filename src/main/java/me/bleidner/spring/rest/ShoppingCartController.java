package me.bleidner.spring.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import me.bleidner.spring.model.Bill;
import me.bleidner.spring.model.ShoppingCart;
import me.bleidner.spring.service.ShoppingCartService;


@RestController
@RequiredArgsConstructor
public class ShoppingCartController {

    public static final String SHOPPING_CARTS_URL = "/shoppingcarts";

    private final ShoppingCartService shoppingCartService;

    @PostMapping(SHOPPING_CARTS_URL)
    public ShoppingCart createNewShoppingCart() {

        return shoppingCartService.create();
    }

    @PostMapping(SHOPPING_CARTS_URL + "/{id}/products/{product_id}")
    public ShoppingCart addProductToCart(@PathVariable long id, @PathVariable("product_id") long productId) {

        return shoppingCartService.addProduct(id, productId);
    }

    @DeleteMapping(SHOPPING_CARTS_URL + "/{id}/products/{product_id}")
    public ShoppingCart removeFromProductFromCart(@PathVariable long id, @PathVariable("product_id") long productId) {

        return shoppingCartService.removeProduct(id, productId);
    }

    @GetMapping(SHOPPING_CARTS_URL + "/{id}/checkout")
    public Bill checkout(@PathVariable long id) {

        return shoppingCartService.checkout(id);
    }

}
