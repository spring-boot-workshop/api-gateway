package me.bleidner.spring.model;

import lombok.Data;


@Data
public class Bill {

    private final long shoppingCartId;
    private final double total;
}
