package me.bleidner.spring.service;

import java.util.List;

import lombok.AllArgsConstructor;
import me.bleidner.spring.model.Product;
import me.bleidner.spring.repository.ProductRepository;


@AllArgsConstructor
public class ProductService {

    private ProductRepository repository;

    public List<Product> getProducts() {

        return repository.getProducts();
    }

    public double getPricesFor(List<Long> productIds) {

        return productIds.stream().map(product -> repository.getProductPrice(product))
                .mapToDouble(Double::doubleValue).sum();
    }
}
