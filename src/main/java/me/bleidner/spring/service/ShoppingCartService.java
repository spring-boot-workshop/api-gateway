package me.bleidner.spring.service;

import lombok.AllArgsConstructor;
import me.bleidner.spring.model.Bill;
import me.bleidner.spring.model.ShoppingCart;
import me.bleidner.spring.repository.ShoppingCartRepository;


@AllArgsConstructor
public class ShoppingCartService {

    private ShoppingCartRepository repository;
    private ProductService service;

    public ShoppingCart create() {

        return repository.create();
    }

    public ShoppingCart addProduct(long shoppingCartId, long productId) {

        return repository.addProduct(shoppingCartId, productId);
    }

    public ShoppingCart removeProduct(long shoppingCartId, long productId) {

        return repository.removeProduct(shoppingCartId, productId);
    }

    public Bill checkout(long shoppingCartId) {

        ShoppingCart shoppingCart = repository.get(shoppingCartId);
        double total = service.getPricesFor(shoppingCart.getProducts());

        return new Bill(shoppingCart.getId(), total);
    }
}
