package me.bleidner.spring.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import me.bleidner.spring.model.Product;
import me.bleidner.spring.repository.ProductRepository;


public class ProductServiceTest {

    private ProductRepository repository;

    private ProductService service;

    @BeforeEach
    void setUp() {

        repository = mock(ProductRepository.class);
        service = new ProductService(repository);
    }

    @Test
    void shouldGetProductsFromRepository() {

        mockResponse();

        List<Product> products = service.getProducts();

        assertThat(products).hasSize(2);
    }

    @Test
    void shouldSumUpPricesOfGivenProductIds() {

        Product mouse = new Product(1L, "Mouse", 30);
        Product keyboard = new Product(2L, "Keyboard", 70);
        mockgetProductPrice(mouse);
        mockgetProductPrice(keyboard);

        double total = service.getPricesFor(List.of(mouse.getId(), keyboard.getId()));

        assertThat(total).isEqualTo(mouse.getPrice() + keyboard.getPrice());
    }

    private void mockgetProductPrice(Product product) {

        when(repository.getProductPrice(eq(product.getId()))).thenReturn(product.getPrice());
    }

    private void mockResponse() {

        List<Product> products = List.of(new Product(1L, "iPhone", 999), new Product(2L, "iPad", 399));
        when(repository.getProducts()).thenReturn(products);
    }
}
