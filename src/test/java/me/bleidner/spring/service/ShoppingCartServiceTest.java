package me.bleidner.spring.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import me.bleidner.spring.model.Bill;
import me.bleidner.spring.model.ShoppingCart;
import me.bleidner.spring.repository.ShoppingCartRepository;


public class ShoppingCartServiceTest {

    private ShoppingCartRepository repository;
    private ShoppingCartService service;
    private ProductService productService;

    @BeforeEach
    void setUp() {

        repository = mock(ShoppingCartRepository.class);
        productService = mock(ProductService.class);
        service = new ShoppingCartService(repository, productService);

    }

    @Test
    void shouldCalculateTotalBasedOnProductService() {
        double total = 100;
        ShoppingCart shoppingCart = new ShoppingCart(1L, List.of(2L, 3L, 4L));
        when(repository.get(anyLong())).thenReturn(shoppingCart);
        when(productService.getPricesFor(eq(shoppingCart.getProducts()))).thenReturn(total);
        Bill bill = service.checkout(1L);

        assertThat(bill.getTotal()).isEqualTo(total);
    }
}
