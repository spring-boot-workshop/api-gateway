package me.bleidner.spring.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;

import me.bleidner.spring.model.Bill;
import me.bleidner.spring.model.ShoppingCart;
import me.bleidner.spring.service.ShoppingCartService;


@WebMvcTest(ShoppingCartController.class)
public class ShopppingCartControllerTest {

    private static final long SHOPPING_CART_ID = 1L;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ShoppingCartService shoppingCartService;

    @Test
    void shouldCreateShoppingCartAndReturnIt() throws Exception {

        when(shoppingCartService.create()).thenReturn(new ShoppingCart(SHOPPING_CART_ID, List.of(3L, 4L)));

        MvcResult result = mvc.perform(post("/shoppingcarts"))
                .andExpect(status().isOk())
                .andReturn();

        ShoppingCart shoppingCart = getShoppingCart(result);

        assertThat(shoppingCart.getId()).isEqualTo(SHOPPING_CART_ID);
        assertThat(shoppingCart.getProducts()).hasSize(2);
    }

    @Test
    void shouldAddProductToShoppingCart() throws Exception {

        when(shoppingCartService.addProduct(anyLong(), anyLong()))
                .thenReturn(new ShoppingCart(SHOPPING_CART_ID, List.of(3L)));

        MvcResult result = mvc.perform(post("/shoppingcarts/1/products/3"))
                .andExpect(status().isOk())
                .andReturn();

        ShoppingCart shoppingCart = getShoppingCart(result);

        assertThat(shoppingCart.getId()).isEqualTo(SHOPPING_CART_ID);
        assertThat(shoppingCart.getProducts()).contains(3L);
        verify(shoppingCartService, times(1)).addProduct(eq(SHOPPING_CART_ID), eq(3L));
    }

    @Test
    void shouldRemoveProductFromShoppingCart() throws Exception {

        when(shoppingCartService.removeProduct(anyLong(), anyLong()))
                .thenReturn(new ShoppingCart(SHOPPING_CART_ID, List.of()));

        MvcResult result = mvc.perform(delete("/shoppingcarts/1/products/3"))
                .andExpect(status().isOk())
                .andReturn();

        ShoppingCart shoppingCart = getShoppingCart(result);

        assertThat(shoppingCart.getProducts()).doesNotContain(3L);
        verify(shoppingCartService, times(1)).removeProduct(eq(SHOPPING_CART_ID), eq(3L));
    }

    @Test
    void shouldReturnBill() throws Exception {

        int total = 100;
        when(shoppingCartService.checkout(anyLong())).thenReturn(new Bill(SHOPPING_CART_ID, total));

        MvcResult result = mvc.perform(get("/shoppingcarts/1/checkout"))
                .andExpect(status().isOk())
                .andReturn();

        Bill bill = getBill(result);

        assertThat(bill.getShoppingCartId()).isEqualTo(SHOPPING_CART_ID);
        assertThat(bill.getTotal()).isEqualTo(total);
    }

    private Bill getBill(MvcResult result)
            throws UnsupportedEncodingException, com.fasterxml.jackson.core.JsonProcessingException {

        String jsonBody = result.getResponse().getContentAsString();
        return mapper.readValue(jsonBody, Bill.class);
    }

    private ShoppingCart getShoppingCart(MvcResult result)
            throws UnsupportedEncodingException, com.fasterxml.jackson.core.JsonProcessingException {

        String jsonBody = result.getResponse().getContentAsString();
        return mapper.readValue(jsonBody, ShoppingCart.class);
    }
}
