package me.bleidner.spring.rest;

import static me.bleidner.spring.rest.ProductController.PRODUCTS_URL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import me.bleidner.spring.model.Product;
import me.bleidner.spring.service.ProductService;


@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ProductService productService;

    @Test
    void shouldReturnProducts() throws Exception {

        mockResponse();

        MvcResult result = mvc.perform(get(PRODUCTS_URL))
                .andExpect(status().isOk())
                .andReturn();

        String jsonBody = result.getResponse().getContentAsString();
        List<Product> products = getProductsFromJson(jsonBody);
        assertThat(products).hasSize(2);
    }

    private List<Product> getProductsFromJson(String body) throws Exception {

        return mapper.readValue(body, new TypeReference<>() {
        });
    }

    private void mockResponse() {

        List<Product> products = List.of(new Product(1L, "iPhone", 999), new Product(2L, "iPad", 399));
        when(productService.getProducts()).thenReturn(products);
    }

}
